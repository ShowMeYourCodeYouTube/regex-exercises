# Exercises

- Write a simple email regex

<details>
  <summary>Answer</summary>
^[\w]+\@{1}[\w\.]+$
</details>

- Match a name followed by Clinton e.g. Hillary Clinton

<details>
  <summary>Answer</summary>
Hillary(?=\s+Clinton)
</details>

- Match a surname which is preceded by Hillary e.g. Hillary Clinton

<details>
  <summary>Answer</summary>
(?<=Hillary\s)\w+
</details>

- Extract day, month, year from DD-MM-YYYY

<details>
  <summary>Answer</summary>
(\d{1,2})-(\d{1,2})-(\d{4})
</details>

- Match any character, number, _ between `aa` and `dd`

<details>
  <summary>Answer</summary>
aa([\w]+)bb
</details>

- Contains Java comment

<details>
  <summary>Answer</summary>
\/\/[^\r\n]*[\r\n]
</details>

- Match the last characters/digits in the line (omit the beginning). The whitespace should be omitted.

<details>
  <summary>Answer</summary>
^(?:\w+\s)*(\w+)$
</details>

---

- Did she say hallo?
  - Write a simple function to check if the string contains the word hallo in different languages.
  - Ref: https://www.codewars.com/kata/56a4addbfd4a55694100001f/train/javascript
- Simple validation of a username with regex
  - Write a simple regex to validate a username. Allowed characters are:
    - lowercase letters,
    - numbers, 
    - underscore 
    - Length should be between 4 and 16 characters (both included).
  - Ref: https://www.codewars.com/kata/56a3f08aa9a6cc9b75000023/train/javascript
- Validate my Password (javascript)
  - I will give you a string. You respond with "VALID" if the string meets the requirements or "INVALID" if it does not. 
    - Passwords must abide by the following requirements:
    - More than 3 characters but less than 20. 
    - Must contain only alphanumeric characters. 
    - Must contain letters and numbers.
  - Ref: https://www.codewars.com/kata/59c01248bf10a47bd1000046/train/javascript
- Can you write an email regex ?
    - Ref: https://stackoverflow.com/questions/8204680/java-regex-email
    - ^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$
- Regular expression for valid decimal or hexadecimal with prefix Ref: https://stackoverflow.com/questions/38247948/regular-expression-for-valid-decimal-or-hexadecimal-with-prefix
  - Desired output:
    1234   (decimal)
    0x12CD (hex)
    0x12cd (hex lowercase)
- javascript multiline regexp replace
  - Ref: https://stackoverflow.com/questions/2575791/javascript-multiline-regexp-replace

