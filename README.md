# Regex overview

Regular expressions are patterns used to match character combinations in strings. [Reference](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions)

Many languages allow programmers to define regexes and then use them to:
- `Validate` that a piece of text (or a portion of that text) matches some pattern
- `Find` fragments of some text that match some pattern
- `Extract` fragments of some text
- `Replace` fragments of text with other text

**Exercises**:
- https://github.com/bonnie/udemy-REGEX/tree/main/exercises/regex101
- https://github.com/ziishaned/learn-regex

**Online testing platform**:
- https://www.hackerrank.com/domains/regex
- https://www.codewars.com/kata/search/?q=regex&beta=false

**Online tools**:
- https://regex101.com/
- https://regexr.com/

## Theory

- Square brackets \[ ] — means exactly one character
- A leading ^ negates, a non-leading, non-terminal - defines a range:
```
[abc]          a or b or c
[^abc] 	       any character _except_ a, b, or c (negation)
[a-zA-Z]       a through z or A through Z, inclusive (range)
```
- Other ways to say exactly one character from a set are:
```
\d             [0-9]
\D             [^\d]
\s             [ \t\n\x0B\f\r]
\S             [^\s]
\w             [a-zA-Z0-9_]
\W             [^\w]
.              any character at all, except maybe not a line terminator
```
- Certain characters, called metacharacters, have special meaning and must be escaped (usually with \ if you want to use them as characters. In most syntaxes the metacharacters are: `(   )   \[   ]  \{   }   ^   $   .   \   ?   *   +   |`
- Some regex tokens do not consume characters! They just assert the matching engine is at a particular place, so to speak:
    - ^: Beginning of string (or line, depending on the mode)
    - $: End of string (or line, depending on the mode)
    - \A: Beginning of string
    - \z: End of string
    - \Z: Varies a lot depending on the engine, so be careful with it
    - \b: Word boundary
    - \B: Not a word boundary

### Quantifiers

![quantifiers](docs/regex-quantifiers.png)

### Lookarounds

![lookarounds](docs/regex-lookarounds.png)


### Groups/Extraction

After doing a match against a pattern, most regex engines will return you a bundle of information, including such things as:
- the part of the text that matched the pattern
- the index within the string where the match begins
- each part of the text matching the parenthesized portions within the pattern
- (sometimes) the text before the matched text
- (sometimes) the text after the matched text

```javascript
const aliceExcerpt = 'The Caterpillar and Alice looked at each other';
const regexpWithoutE = /\b[a-df-z]+\b/ig;
console.log(aliceExcerpt.match(regexpWithoutE));
// expected output: Array ["and", "at"]

const imageDescription = 'This image has a resolution of 1440×900 pixels.';
const regexpSize = /([0-9]+)×([0-9]+)/;
const match = imageDescription.match(regexpSize);
console.log(`Width: ${match[1]} / Height: ${match[2]}.`);
// expected output: "Width: 1440 / Height: 900."
```

### Examples

![examples](docs/regex-examples.png)


### References

- https://cs.lmu.edu/~ray/notes/regex/
- https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Groups_and_Ranges

## Miscellaneous

- What is the difference between .*? and .* regular expressions?
  - The .* pattern is greedy, meaning it tries to match as much of the input text as possible.
  - Non-Greedy (or Lazy) Matching: The .*? pattern is non-greedy, meaning it tries to match as little of the input text as possible while still allowing the overall pattern to succeed.
  - Example 1: From A to Z
    - Given the following input: `eeeAiiZuuuuAoooZeeee`
      - The patterns yield the following matches:
      - A.*Z yields 1 match: AiiZuuuuAoooZ
      - A.*?Z yields 2 matches: AiiZ and AoooZ
  - Example 2: From A to ZZ
    - This example should be illustrative: it shows how the greedy, reluctant, and negated character class patterns match differently given the same input.
    - Given the following input: `eeAiiZooAuuZZeeeZZfff`.
    - These are the matches for the above input:
      - A[^Z]*ZZ yields 1 match: AuuZZ
      - A.*?ZZ yields 1 match: AiiZooAuuZZ
      - A.*ZZ yields 1 match: AiiZooAuuZZeeeZZ
  - Ref: <https://stackoverflow.com/questions/3075130/what-is-the-difference-between-and-regular-expressions>
- Greedy and non-greedy quantifiers
  - By default, quantifiers work in the greedy mode. It means the greedy quantifiers will match their preceding elements as much as possible to return to the biggest match possible.
  - On the other hand, the non-greedy quantifiers will match as little as possible to return the smallest match possible
  - Ref: <https://www.pythontutorial.net/python-regex/python-regex-non-greedy/>
- What are positive & negative lookahead/lookbehind?
  - Lookahead assertions (ECMAScript 3):
    - Positive lookahead: (?=«pattern») matches if pattern matches what comes after the current location in the input string.
    - Negative lookahead: (?!«pattern») matches if pattern does not match what comes after the current location in the input string.
    - Lookbehind assertions (ECMAScript 2018):
      - Positive lookbehind: (?<=«pattern») matches if pattern matches what comes before the current location in the input string.
      - Negative lookbehind: (?<!«pattern») matches if pattern does not match what comes before the current location in the input string.
  - Example
    - 'how "are" "you" doing'.match(/[?<="](a-z)+(?=")/g)
    - [ 'are', 'you' ]
    - Two lookaround assertions help us here:
      - (?<=") “must be preceded by a quote”
      - (?=") “must be followed by a quote”
    - Pattern with Lookahead: `A(?=B)`
    - Pattern with Negative Lookahead: A(?!B)
    - Pattern with Lookbehind: `(?<=A)B`
  - References:
    - <https://exploringjs.com/deep-js/ch_regexp-lookaround-assertions.html>
    - <https://coderwall.com/p/5c7kjq/lookahead-and-lookbehind-regex>
    - <https://stackoverflow.com/questions/2973436/regex-lookahead-lookbehind-and-atomic-groups>
- What is a group in Regex?
  - A group is a part of a regex pattern enclosed in parentheses () metacharacter. We create a group by placing the regex pattern inside the set of parentheses ( and ) . For example, the regular expression (cat) creates a single group containing the letters ‘c’, ‘a’, and ‘t’.
  - Example:
  - let regexp = /(\w+\.)+\w+/g;
  - alert("site.com my.site.com".match(regexp)); // site.com,my.site.com
